<?php

require_once 'core.php';

use KalebKlein\Flash;

// Set a message stating if the user denied the authorization
if(isset($_GET['error']))
{
	// throw error messages
	if($_GET['error_reason'] == 'user_denied')
		Flash::set('global', 'You have aborted the Facebook authorization!');
}
else
{
	// User accepted authorization, log them in
	try
	{
		$fb->setSession($fb->getHelper()); // Creates the FB session for use
	}
	catch(Exception $e)
	{
		echo $e->getMessage();
		exit;
	}

	// If the FB session was created, register the site session
	if($fb->getSession())
	{
		$fb->registerUserInfoIntoSession();
		Flash::set('global', 'You have successfully logged in!');
	}
}

// Redirect to the home page
header('Location: index.php');
