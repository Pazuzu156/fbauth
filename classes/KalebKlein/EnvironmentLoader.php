<?php namespace KalebKlein;

class EnvironmentLoader
{
	private $_env;

	/**
	 * class constructor
	 * @param $envFile
	 */
	public function __construct($envFile)
	{
		$this->extract($envFile);
	}

	/**
	 * extracts the environment variables from
	 * the supplied env file
	 * @param $envFile
	 */
	private function extract($envFile)
	{
		$file = file($envFile);
		foreach($file as $item)
		{
			if(!empty($item))
			{
				$exp = explode('=', $item);
				$this->_env[trim($exp[0])] = trim($exp[1]);
			}
		}
	}

	/**
	 * gets an environment variable
	 * @param $id
	 * @return mixed
	 */
	public function get($id)
	{
		return $this->_env[$id];
	}
}
