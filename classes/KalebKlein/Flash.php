<?php namespace KalebKlein;

/**
 * Class Flash
 * @package KalebKlein
 */
class Flash
{
	/**
	 * sets the flash message
	 * @param $id
	 * @param $message
	 */
	public static function set($id, $message)
	{
		$_SESSION[$id] = $message;
	}

	/**
	 * gets a flash message
	 * @param $id
	 * @return mixed
	 * @throws \Exception
	 */
	public static function get($id)
	{
		if(self::exists($id))
		{
			$message = $_SESSION[$id];
			self::delete($id);
			return $message;
		}
		else
			throw new \Exception("You do not have a flash message set with that ID!");
	}

	/**
	 * deletes a flash message
	 * @param $id
	 */
	public static function delete($id)
	{
		unset($_SESSION[$id]);
	}

	/**
	 * checks if a flash message exists
	 * @param $id
	 * @return bool
	 */
	public static function exists($id)
	{
		return (isset($_SESSION[$id])) ? true : false;
	}
}
