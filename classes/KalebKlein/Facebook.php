<?php namespace KalebKlein;

use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequestException;
use Facebook\FacebookRequest;

/**
 * Class Facebook
 * @package KalebKlein
 */
class Facebook
{
	private $_helper, $_session;

	/**
	 * class constructor
	 * @param $appId
	 * @param $appSecret
	 */
	public function __construct($appId, $appSecret)
	{
		FacebookSession::setDefaultApplication($appId, $appSecret);
	}

	/**
	 * registers the login helper
	 * @param $redirectUrl
	 */
	public function registerLoginHelper($redirectUrl)
	{
		$this->_helper = new FacebookRedirectLoginHelper(
			$redirectUrl);
	}

	/**
	 * gets the login URL
	 * @return mixed
	 */
	public function getLoginUrl()
	{
		return $this->_helper->getLoginUrl();
	}

	/**
	 * gets the login helper
	 * @return mixed
	 */
	public function getHelper()
	{
		return $this->_helper;
	}

	/**
	 * gets the FB session
	 * @return mixed
	 */
	public function getSession()
	{
		return $this->_session;
	}

	/**
	 * sets the FB session from the login helper
	 * @param FacebookRedirectLoginHelper $helper
	 * @throws \Exception
	 */
	public function setSession(FacebookRedirectLoginHelper $helper)
	{
		try
		{
			$this->_session = $helper->getSessionFromRedirect();
		}
		catch(FacebookRequestException $ex)
		{
			throw new \Exception('Failed to get session from Facebook. Error: ' . $ex->getMessage());
		}
		catch(\Exception $ex)
		{
			throw new \Exception('Internal exception. Error: ' . $ex->getMessage());
		}
	}

	/**
	 * sets the FB session from the user's auth token
	 * @param $sessionToken
	 */
	private function setSessionFromToken($sessionToken)
	{
		$this->_session = new FacebookSession($sessionToken);
	}

	/**
	 * sets the site session for storing logged in user's auth token
	 */
	public function registerUserInfoIntoSession()
	{
		$_SESSION['facebook'] = $this->_session->getToken();
	}

	/**
	 * checks if the user is currently logged in
	 * @return bool
	 */
	public function loggedIn()
	{
		return (isset($_SESSION['facebook'])) ? true : false;
	}

	/**
	 * get's user's info from FB.
	 * Optional parameters allow for more options on the Graph API
	 *
	 * Refer to the Graph API reference guide on developers.facebook.com
	 * for more info
	 * @param $get
	 * @param string $id
	 * @param string $item
	 * @param array $params
	 * @return mixed
	 * @throws FacebookRequestException
	 */
	public function get($get, $id="", $item="", $params=array())
	{
		// This little tid-bit creates a string
		// of parameters that will be passed on to
		// facebook if extra parameters are needed
		$end = "";
		if(!empty($params))
		{
			$end = "?";
			foreach($params as $key => $value)
				$end .= $key.'='.$value.'&';
		}

		// Header simply enhances the calling scheme. If certain parameters are
		// required, then the header will do it's job. Else, just the standard
		// /me is tacked on
		$headers = (empty($id)) ? '/me' : '/' . $id . '/' . $item . '/' . rtrim($end, '&');
		$this->setSessionFromToken($_SESSION['facebook']);
		$graph = (new FacebookRequest($this->getSession(), 'GET', $headers))
			->execute()->getGraphObject();

		// Returns the property needed from the Graph API Object
		return $graph->getProperty($get);
	}

	/**
	 * logs the user out
	 */
	public function logout()
	{
		// If the user is logged in, remove the
		// facebook session
		if($this->loggedIn())
			unset($_SESSION['facebook']);

		Flash::set('global', 'You have been successfully logged out!');

		header('Location: index.php');
	}
}
