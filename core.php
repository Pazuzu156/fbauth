<?php

session_start(); // init sessions

// Load in all required classes
require_once __DIR__.'/autoload.php';

// Load environment class
$env = new KalebKlein\EnvironmentLoader(__DIR__.'/.env');

// FB App Info
$appId = $env->get('APP_ID');
$appSecret = $env->get('APP_SECRET');

// Create Facebook instance
$fb = new KalebKlein\Facebook($appId, $appSecret);
$fb->registerLoginHelper($env->get('REDIRECT_URL'));
