# FBAuth
PHP implementation of the Facebook auth system for login

## Install
Clone the repo, and install the Facebook PHP SDK using composer

```$ composer install```

## Running
Use the AppID and AppSecret tokens Facebook gives you from [Facebook Developers](http://developers.facebook.com)  
Place them in the .env

Since the .env file doesn't exist when cloned, simply rename .env.example and use it

You're all set up now!
