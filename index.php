<?php

require_once 'core.php';
use KalebKlein\Flash;

// Obtain the users FB ID if the user is logged in
$id = ($fb->loggedIn()) ? $fb->get('id') : '';

?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Facebook Login</title>
	</head>
	<body>
		<?= (Flash::exists('global')) ? Flash::get('global')."<br>" : ""; ?>
		<?php if($fb->loggedIn()): ?>
		Welcome, <img src="<?= $fb->get('url', $id, 'picture', array('redirect' => 'false', 'type' => 'large')); ?>" style="width: 40px;"> <?= $fb->get('name'); ?>!
		<br>
		<a href="logout.php">Logout</a>
		<hr>
		<br>
		<h3>Statuses</h3>
		<?php

		// This just gets the most recent status updates from the
		// user's Timeline
		$statuses = $fb->get('data', $id, 'statuses')->asArray();

		foreach($statuses as $status)
		{
			echo $status->message . "<br><br>";
		}

		?>
		<?php else: ?>
		You are not logged in!<br>
		<a href="<?php echo $fb->getLoginUrl(); ?>">Login</a>
		<?php endif; ?>
	</body>
</html>
