<?php

// Load in Facebook package
require_once __DIR__.'/vendor/autoload.php';

// Load in modules for local package
spl_autoload_register(function($class) {
	require_once __DIR__.'/classes/'.str_replace('\\', '/', $class).'.php';
});
